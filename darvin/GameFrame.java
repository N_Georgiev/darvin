package darvin;

import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameFrame extends Frame {
	Karta karta;
	GamePanelToolbox panelToolbox;
	GamePanelMap panelMap;
	Label label;
	TextField textField;
	TextArea textArea;
	Button button;
	
	GameFrame(Karta k) {
		karta = k;
		GameWindowListener gwl = new GameWindowListener();
		addWindowListener(gwl);
		this.setLayout(null);
		
		panelToolbox = new GamePanelToolbox();
		panelToolbox.setBounds(karta.sizex*75 + 1 + 300, 75, 200 , 700 );
		panelToolbox.setLayout(null);
		this.add(panelToolbox);
		
		panelMap = new GamePanelMap(k);
		panelMap.setBounds(75, 75, karta.sizex*75 + 1, karta.sizey*75 + 1);
		panelMap.setBackground(Color.white);
		panelMap.setLayout(null);
		this.add(panelMap);
		}
	
	
}

