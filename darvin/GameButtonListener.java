package darvin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameButtonListener implements ActionListener{
	GamePanelToolbox panel;
	
	GameButtonListener(GamePanelToolbox p) {
		panel = p;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String s = panel.textField.getText();
		panel.label.setText(s);
	}

}
