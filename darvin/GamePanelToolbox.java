package darvin;

import java.awt.Button;
import java.awt.Color;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;

public class GamePanelToolbox extends Panel{
	Label label;
	TextField textField;
	TextArea textArea;
	Button button;
	
	GamePanelToolbox() {
		label = new Label();
		label.setBounds(0, 0, 200, 50);
		label.setText("Hello");
		label.setBackground(Color.LIGHT_GRAY);
		this.add(label);
		
		textField = new TextField();
		textField.setBounds(0, 100, 200, 50);
		this.add(textField);
		
		textArea = new TextArea();
		textArea.setBounds(0, 200, 200, 150);
		this.add(textArea);
		
		GameButtonListener gbl = new GameButtonListener(this);
		
		button = new Button();
		button.setBounds(0, 450, 200, 150);
		button.setLabel("Започни играта");
		button.addActionListener(gbl);
		this.add(button);
	}
}

