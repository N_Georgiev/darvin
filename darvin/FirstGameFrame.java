package darvin;

import java.awt.Color;
import java.awt.Frame;
import java.awt.TextArea;


public class FirstGameFrame extends Frame {
	TextArea textArea;
	FirstGameFrame() {
		GameWindowListener gwl = new GameWindowListener();
		addWindowListener(gwl);
		this.setLayout(null);
		textArea = new TextArea();
		textArea.setLocation(1, 1);
		textArea.setSize(1500, 750);
		textArea.setBackground(Color.black);
		textArea.setFont(getFont());
		textArea.setText(Instructions.getFirstInstruction());
		this.add(textArea);
	}
	
}
