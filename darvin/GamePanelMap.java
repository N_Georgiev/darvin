package darvin;

import java.awt.Button;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GamePanelMap extends Panel{
	Karta karta;
	Label label;
	TextField textField;
	TextArea textArea;
	Button button;
	
	GamePanelMap(Karta k) {
		karta = k;
		GameKeyListener gkl = new GameKeyListener(this);
		addKeyListener(gkl);
		GameMouseListener gml = new GameMouseListener(this);
		addMouseListener(gml);
	}
	public void paint(Graphics g) {
		System.out.println(karta.sizey);
		
		BufferedImage img = null;
		File f = new File("screenshot.png");
		try {
			img = ImageIO.read(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		g.drawImage(img,karta.hero.getPosX() * 75 ,karta.hero.getPosY() * 75, 75, 75,  null);
		for (int i = 0; i < karta.sizex; i++) {
			for (int j = 0; j < karta.sizey; j++) {
				g.drawRect(i * 75 ,j * 75,  75,  75);
			}
		}
	}
}
