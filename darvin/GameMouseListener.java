package darvin;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GameMouseListener implements MouseListener {
	GamePanelMap panel;
	
	GameMouseListener(GamePanelMap p) {
		panel = p;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		panel.karta.hero.setPosX(e.getX() / 75);
		panel.karta.hero.setPosY(e.getY() / 75);
		panel.repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}

