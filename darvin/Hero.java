package darvin;

public class Hero {
	private int posX;
	private int posY;
	public String name;
	public char sign;
	public Karta k;

	
	public Hero (String n, int x, int y, Karta k)  throws Exception {
		name = n;
		sign = name.charAt(0);
		posX = x;
		posY = y;
		this.k = k;
		if (x < 0) {
			throw new Exception();
		}
		
		if (y < 0) {
			throw new Exception();
		}

	}
	
	/// getters-----------------------------------------------------------------------------------------------------------------------------------------------------
	public int getPosX () {
		return posX;
	}

	public int getPosY() {
		return posY;
	}
	
	/// setters----------------------------------------------------------------------------------------------------------------------------------------------
	public void setPosX(int posX) {
		if (posX >= 0 && posX < k.sizex) {
			this.posX = posX;
		} 
	}

	public void setPosY(int posY) {
		if (posY >= 0 && posY < k.sizey) {
			this.posY = posY;
		}
	}
	
	/// функции за придвижване-------------------------------------------------------------------------------------------------------------------------------
	
	public void dvizhiSe(char c) {
		switch(c) {
		case 'w': setPosY(posY-1); break;
		case 'a': setPosX(posX-1); break;
		case 's': setPosY(posY+1); break;
		case 'd': setPosX(posX+1); break;
		}
	}
	
//	public void nagore () {
//		posY--;
//		if (posY < 0) {
//			posY = k.sizey - 1;
//		}
//		System.out.println(posY);
//	}
//	
//	public void nadolu () {
//		posY++;
//		if (posY > k.sizey - 1) {
//			posY = 0;
//		}
//		System.out.println(posY);
//	}
//	
//	public void nalqvo () {
//		posX--;
//		if (posX < 0) {
//			posX = k.sizex - 1;
//		}
//		System.out.println(posX);
//	}
//	
//	public void nadqsno () {
//		posX++;
//		if (posX > k.sizex - 1) {
//			posX = 0;
//		}
//		System.out.println(posX);
//	}

	
//	void move(char c) {
//		switch(c) {
//			case 'w': posy--; break;
//			case 's': posy++; break;
//			case 'a': posx--; break;
//			case 'd': posx++; break;
//		}
//	}
}

