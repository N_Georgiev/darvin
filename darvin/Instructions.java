package darvin;

import java.util.Scanner;

public class Instructions {
	public static String getFirstInstruction() {
		return 	"Здравей Дарвин, току-що ти уби Гъмбол. " +
				"Единственият начин да върнеш живота му е да изпълниш поставените предизвикателства. " +
				"Готов ли си? " + 
				"Ще започнеш с максимум здраве, сила и брой на животи. " + 
				"Първият му параметър е храната, с която разполага. Тя може да бъде от 0 до 100. " + 
				"Вторият му параметър е силата на удара, която има. Тя може да бъде от 0 до 100. " + 
				"Третият и последен параметър е броят на животите, които героят има. Те не могат да бъдат повече от 3. ";
	}
	
	public static void printSecondInstruction(Scanner scan) {
		System.out.println("Сега ще ти покажа картата и къде се намираш на нея.");
		scan.nextLine();
		System.out.println("Въведи размера на картата и твоята позиция на нея.");
	}
	
	public static void printThirdInstruction(Scanner scan) {
		scan.nextLine();
		System.out.println("Първото предизвикателство е да се изправиш срещу един от най-страшните дяволи - акуло-мечко-гатора.");
		scan.nextLine();
		System.out.println("Той обединява силите на риба, бозайник и влечуго, така че бъди нащрек за всеки негов удар.");
		scan.nextLine();
		System.out.println("За да намериш слабото му място, трябва да се сетиш какво е любимото нещо на Ричард - да рови в хладилника.");
		scan.nextLine();
		System.out.println("Пред теб се появява хладилник, отвори го, за да привлечеш вниманието на врага си.");
		scan.nextLine();
		System.out.println("Не забравяй, че той е и три пъти по-гладен от тебе.");
		scan.nextLine();
		System.out.println("И в момента, в който врагът ти отвори хладилника, тогава трябва да нанесеш финалния си удар.");
		
	}
	
	static void printFourthInstruction() {
		System.out.println("Следващото предизвикателство представлява оцеляване в гората.");
		System.out.println("Добре си спомняш, когато бяхте на ученическа екскурзия ");
		System.out.println("");
	}

}

